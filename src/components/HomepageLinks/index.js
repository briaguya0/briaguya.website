import React from 'react';
import clsx from 'clsx';
import IconExternalLink from '@theme/IconExternalLink';
import Link from '@docusaurus/Link';

const LinksList = [
  // {
  //   title: 'docs',
  //   url: '/docs/docs'
  // },
  // {
  //   title: 'blog',
  //   url: 'blog'
  // },
  // {
  //   title: 'secret page',
  //   url: '#todo-make-a-secret-page'
  // },
  {
    title: 'gitlab',
    url: 'https://gitlab.com/briaguya0',
    external: true,
  },
  {
    title: 'github',
    url: 'https://github.com/briaguya-ai',
    external: true,
  },
  // {
  //   title: 'twitch',
  //   url: 'https://www.twitch.tv/briaguya0',
  //   external: true,
  // },
];

function HomepageLink({title, url, external}) {
  return (
    <div className={clsx('col col--6')}>
      <div className="text--center padding-horiz--md padding-vert--sm">
        <Link to={url} className="navbar__link"><h2><span>{title} {external && <IconExternalLink width={20} height={20} />}</span></h2></Link>
      </div>
    </div>
  );
}

export default function HomepageLinks() {
  return (
    <section>
      <div className="container">
        <div className="row">
          {LinksList.map((props, idx) => (
            <HomepageLink key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
