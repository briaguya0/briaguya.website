---
sidebar_position: 1
---

# docs

## why docs?

i learned a lot of tricks when working on the retroachivements bonus set for oot

a lot of what i learned required watching videos, pausing, scrubbing back and forth, and listening to an audio description of how to perform the tricks

while this did work, and (with the help of twitch chat) i was able to complete the bonus set, i felt like there must be a better way to learn these things

so i'm trying to make one

i haven't fully fleshed out what i want it to look like yet, but it should be a good guide for someone learning tricks for the first time, and good reference material for anyone who wants a refresher
