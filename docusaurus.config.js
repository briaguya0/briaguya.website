// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'briaguya',
  url: 'https://briaguya.website',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  projectName: 'briaguya.website', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        // docs: {
        //   sidebarPath: require.resolve('./sidebars.js'),
        //   // https://gitlab.com/briaguya0/briaguya.website
        //   editUrl: 'todo',
        // },
        // blog: {
        //   showReadingTime: true,
        //   editUrl:
        //     'todo',
        // },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "briaguya.website",
        logo: {
          alt: "sig",
          src: "img/sig.png"
        },
        items: [
          // {
          //   type: "doc",
          //   docId: "docs",
          //   position: "left",
          //   label: "docs"
          // },
          // {to: '/blog', label: 'blog', position: 'left'},
          {
            href: 'https://gitlab.com/briaguya0/briaguya.website',
            label: 'website source',
            position: 'right',
          },
        ],
      },
      colorMode: {
        defaultMode: 'dark',
        disableSwitch: true,
        respectPrefersColorScheme: false,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
